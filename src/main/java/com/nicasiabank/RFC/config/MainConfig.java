package com.nicasiabank.RFC.config;

import com.nicasiabank.RFC.MainPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author rajim 2021-10-03.
 * @project IntelliJ IDEA
 */

/**
 * Import all configuration file here
 */
@Configuration
@Import(value = {
        DataSourceConfig.class,
        SecurityConfig.class
})
/**
 * Define level of class scan
 */
@ComponentScan(basePackageClasses = {
        MainPackage.class
})
public class MainConfig {
}
