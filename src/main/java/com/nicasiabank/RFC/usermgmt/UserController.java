package com.nicasiabank.RFC.usermgmt;

import com.nicasiabank.RFC.usermgmt.model.Roles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
@RestController
@RequestMapping(value = "/api/users")
public class UserController {


    @GetMapping
    public String test() {
        return "Hello from user";
    }

}
