package com.nicasiabank.RFC.usermgmt.model;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
public enum Roles {
    SUPER_ADMIN, ADMIN, ADMIN_USER
}
