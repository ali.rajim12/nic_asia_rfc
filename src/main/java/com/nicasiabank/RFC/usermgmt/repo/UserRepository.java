package com.nicasiabank.RFC.usermgmt.repo;

import com.nicasiabank.RFC.usermgmt.model.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
@Repository
public interface UserRepository
        extends CrudRepository<Users, Long> {

    @Query("select u from Users  u where u.username=?1 and u.status=1")
    Optional<Users> findByUsername(String username);
}
