package com.nicasiabank.RFC.authentication.service;

import com.nicasiabank.RFC.authentication.dto.LoginRequest;
import com.nicasiabank.RFC.authentication.dto.LoginResource;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
public interface AuthenticationService {
    LoginResource createLogin(LoginRequest loginRequest);
}
