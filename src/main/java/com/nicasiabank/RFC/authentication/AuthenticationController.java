package com.nicasiabank.RFC.authentication;

import com.nicasiabank.RFC.authentication.dto.LoginRequest;
import com.nicasiabank.RFC.authentication.dto.LoginResource;
import com.nicasiabank.RFC.authentication.service.AuthenticationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
@RestController
@RequestMapping(value = "/api/login")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }


    @PostMapping
    public LoginResource login(@RequestBody LoginRequest loginRequest) {
        return authenticationService.createLogin(loginRequest);
    }


}
