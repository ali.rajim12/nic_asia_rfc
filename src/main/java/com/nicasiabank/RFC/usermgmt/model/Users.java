package com.nicasiabank.RFC.usermgmt.model;

import com.nicasiabank.RFC.abstracts.AbstractEntity;
import com.nicasiabank.RFC.shared.Status;
import lombok.Data;

import javax.persistence.Entity;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
@Data
@Entity
public class Users extends AbstractEntity {

    private String fullName;

    private String username;

    private String password;

    private String email;

    private String mobileNumber;

    private Status status;

    private Roles roles;

    // 0, 1, 2

}
