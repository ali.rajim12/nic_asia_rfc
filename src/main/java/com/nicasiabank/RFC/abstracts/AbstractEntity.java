package com.nicasiabank.RFC.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author rajim 2021-10-03.
 * @project IntelliJ IDEA
 */
@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public abstract class AbstractEntity extends AbstractPersist<Long> {

    @JsonIgnore
    @Getter
    @Setter(AccessLevel.PROTECTED)
    @CreatedDate
    private Date created = new Date();

    @JsonIgnore
    @Getter
    @Setter(AccessLevel.PROTECTED)
    @LastModifiedDate
    private Date lastModified;

    @Version
    private Long version = 1L;

    @CreatedBy
    @Column(updatable = false)
    private Long createdByUserId;

    @LastModifiedBy
    private Long updatedByUserId;

    @JsonIgnore
    @Override
    public boolean isNew() {
        return super.isNew();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    @PrePersist
    public void prePersist() {
        this.lastModified = new Date();
    }
}

