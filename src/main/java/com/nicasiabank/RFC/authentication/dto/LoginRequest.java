package com.nicasiabank.RFC.authentication.dto;

import lombok.Data;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
@Data
public class LoginRequest {
    private String username;
    private String password;
}
