package com.nicasiabank.RFC.usermgmt.service;

import com.nicasiabank.RFC.usermgmt.model.Users;

import java.util.Optional;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
public interface UserService {
    Optional<Users> findByUserName(String username);
}
