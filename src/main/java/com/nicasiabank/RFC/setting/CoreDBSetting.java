package com.nicasiabank.RFC.setting;

import com.nicasiabank.RFC.abstracts.AbstractDBSetting;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author rajim 2021-10-03.
 * @project IntelliJ IDEA
 */
@Component
@ConfigurationProperties("datasource.core")
public class CoreDBSetting extends AbstractDBSetting {
}
