package com.nicasiabank.RFC.abstracts;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.cfg.DefaultNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.io.Serializable;

/**
 * @author rajim 2021-10-03.
 * @project IntelliJ IDEA
 */
public class OldNamingConvention extends PhysicalNamingStrategyStandardImpl implements Serializable {

    public static final org.hibernate.cfg.DefaultNamingStrategy INSTANCE = new DefaultNamingStrategy();

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        return new Identifier(addUnderscores(name.getText()), name.isQuoted());
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        return new Identifier(addUnderscores(name.getText()), name.isQuoted());
    }

    protected static String addUnderscores(String name) {
        return name;
    }
}
