package com.nicasiabank.RFC.config;

import com.nicasiabank.RFC.MainPackage;
import com.nicasiabank.RFC.abstracts.AbstractDataConfig;
import com.nicasiabank.RFC.setting.CoreDBSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * @author rajim 2021-10-03.
 * @project IntelliJ IDEA
 */

@Configuration
@ComponentScan(basePackageClasses = {MainPackage.class})
@EnableJpaRepositories(basePackageClasses = {MainPackage.class},
        entityManagerFactoryRef = "rfcEMF",
        transactionManagerRef = "rfcTM")
@EnableSpringDataWebSupport
public class DataSourceConfig extends AbstractDataConfig {

    private static final String PU_NAME = "rfc";
    private static final String DATASOURCE_NAME = "rfcDataSource";

    // TO add multiple

//    private static final String EMS_REPORTS_DS = "emsReportsDS";
//    private static final String EMS_REPORTS_JDBC = "emsReportsJdbc";


    private static final String[] PACKAGES = {MainPackage.class.getPackage().getName()};

    @Autowired
    @Bean(name = DATASOURCE_NAME)
    public DataSource coreDataSource(final CoreDBSetting coreDBSetting) {
        return buildDataSource(coreDBSetting);
    }

    @Bean(name = "rfcEMF")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @Qualifier(value = DATASOURCE_NAME) DataSource dataSource) {
        return buildEntityManagerFactory(dataSource, PU_NAME, PACKAGES);
    }

    @Bean(name = "rfcTM")
    public PlatformTransactionManager transactionManager(
            final @Qualifier(value = "rfcEMF") EntityManagerFactory emf) {
        return buildTransactionManager(emf);
    }


    // Need if we want to add extra datasource

//    @Bean(name = EMS_REPORTS_DS)
//    public DataSource newPlusTwoDs(final EMSReportingDataSourceSetting emsReportingDataSourceSetting) {
//        return buildDataSource(emsReportingDataSourceSetting);
//    }
//
//    @Bean(name = EMS_REPORTS_JDBC)
//    public NamedParameterJdbcTemplate newPlusTwoJdbc(@Qualifier(EMS_REPORTS_DS) DataSource emsDataSource ) {
//        return new NamedParameterJdbcTemplate(emsDataSource);
//    }


}
