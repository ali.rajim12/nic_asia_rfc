package com.nicasiabank.RFC.shared.startup;

import com.nicasiabank.RFC.shared.Status;
import com.nicasiabank.RFC.usermgmt.model.Roles;
import com.nicasiabank.RFC.usermgmt.model.Users;
import com.nicasiabank.RFC.usermgmt.repo.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
@Component
public class UserCreator {
    Logger logger = LoggerFactory.getLogger(UserCreator.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public UserCreator(UserRepository userRepository,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void createSuperAdminUser() {
        if(userRepository.count() == 0) {
            logger.info("Default user is creating ....");
            createUser();
        }
    }

    private void createUser() {
        Users users = new Users();
        users.setFullName("Rajim");
        users.setEmail("ali.rajim12@gmail.com");
        users.setMobileNumber("9849428177");
        users.setRoles(Roles.SUPER_ADMIN);
        users.setUsername("9849428177");
        users.setStatus(Status.ACTIVE);
        users.setPassword(passwordEncoder.encode("test12"));
        userRepository.save(users);
    }
}
