package com.nicasiabank.RFC.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nicasiabank.RFC.usermgmt.model.Users;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
@Data
public class UserPrincipal implements UserDetails {

    private final Users users;

    private Long id;

    private String name;

    private String username;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal(Long id,
                         String name,
                         String username,
                         String password,
                         Collection<? extends GrantedAuthority> authorities,
                         Users users) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.users = users;
    }

    public static UserPrincipal create(Users user) {
        // TO add user authorities we need to setup user roles
        List<GrantedAuthority> authorities = Arrays.asList(user.getRoles())
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.name())
        ).collect(Collectors.toList());
        return new UserPrincipal(
                user.getId(),
                user.getFullName(),
                user.getUsername(),
                user.getPassword(),
                authorities,
                user
        );
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
