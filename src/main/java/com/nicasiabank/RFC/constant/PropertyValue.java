package com.nicasiabank.RFC.constant;

/**
 * @author rajim 2021-10-03.
 * @project IntelliJ IDEA
 */
public class PropertyValue {
    public static final String CORS_URL = "${cors.url}";

    public static final String FILE_PATH = "file:${catalina.home}/conf/rfc_prop.yml";

    // jwt config
    public static final String JWT_SECRET = "${jwt.secret}";

    public static final String JWT_EXPIRY_TIME_INMLIS = "${jwt.expiry}";


}
