package com.nicasiabank.RFC.usermgmt.service;

import com.nicasiabank.RFC.usermgmt.model.Users;
import com.nicasiabank.RFC.usermgmt.repo.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<Users> findByUserName(String username) {
        return userRepository.findByUsername(username);
    }
}
