package com.nicasiabank.RFC.authentication.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class LoginResource {
    private String fullName;
    private String username;
    private String role;
    private String token;
}
