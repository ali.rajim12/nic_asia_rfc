package com.nicasiabank.RFC.shared;

/**
 * @author rajim 2021-10-04.
 * @project IntelliJ IDEA
 */
public enum Status {
    INACTIVE, ACTIVE, DELETED
}
