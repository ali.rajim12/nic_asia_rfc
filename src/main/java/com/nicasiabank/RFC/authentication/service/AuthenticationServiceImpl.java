package com.nicasiabank.RFC.authentication.service;

import com.nicasiabank.RFC.authentication.dto.LoginRequest;
import com.nicasiabank.RFC.authentication.dto.LoginResource;
import com.nicasiabank.RFC.security.JwtTokenProvider;
import com.nicasiabank.RFC.usermgmt.model.Users;
import com.nicasiabank.RFC.usermgmt.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * @author rajim 2021-10-05.
 * @project IntelliJ IDEA
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider tokenProvider;

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    public AuthenticationServiceImpl(AuthenticationManager authenticationManager,
                                     JwtTokenProvider tokenProvider,
                                     UserService userService,
                                     PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public LoginResource createLogin(LoginRequest loginRequest) {
        logger.info("Login request from :: {}", loginRequest.getUsername());
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),
                            (loginRequest.getPassword())
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = tokenProvider.generateToken(authentication);
            Users user = userService.findByUserName(loginRequest.getUsername())
                    .orElse(null);
            return LoginResource.builder()
                    .fullName(user.getFullName())
                    .role(user.getRoles().name())
                    .username(user.getUsername())
                    .token(token)
                    .build();

        } catch (Exception e) {
//            throw new ClientException("Invalid Login");
        }
        return null;
    }
}
